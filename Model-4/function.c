
#include <stdio.h>

// convert a decimal number to a binary number by using the function

int tobinary(short int dno){
   int bno=0,rem,f=1;
   while(dno != 0){
      rem = dno % 2;
      bno = bno + rem * f;
      f = f * 10;
      dno = dno / 2;
   }
   return bno;;
}

int Zero(int number, short int size) {

  char text[size];
  sprintf(text, "%d", number);

  short int  i;
  short int add = 0;

  for (i = 0; i < size; i++) {
    if (text[i] == '0') {
      add++;
    }
  }
  return add;
}


