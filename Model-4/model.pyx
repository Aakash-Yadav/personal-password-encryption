
from ctypes import CDLL 
from cpython cimport array 
from functools import reduce

## Calling Cfunctions in Cython file 

Cfunctions = CDLL("./Cfun.so")


cdef convert_password_to_int(str password,short int length):
    cdef short int i
    cdef array.array password_array = array.array('i',[0 for i in range(length)]);
    for i in range(length):
        password_array[i] = ord(password[i]);
    return password_array

cdef convert_password_int_array_to_bin(str password,short int length):
    cdef array.array password_int_array = convert_password_to_int(password,length);
    
    cdef short int i 

    cdef array.array binary_array  = array.array('i',[0 for i in range(length)]);

    for i in range(length):
        binary_array[i] = Cfunctions.tobinary(password_int_array[i])
    return binary_array;

cpdef get_the_ratio(str password,short int length):

    cdef array.array binary_array_of_password = convert_password_int_array_to_bin(password,length);
    cdef array.array result = array.array("i",[]);


    cdef short int i,zero,one;

    for i in range(length):
        zero = (Cfunctions.Zero(binary_array_of_password[i],7));
        one =   7-zero;

    pass 










