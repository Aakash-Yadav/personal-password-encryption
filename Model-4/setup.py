
from setuptools import setup
from Cython.Build import cythonize

setup(
    ext_modules = cythonize("model.pyx")
)

from os import listdir,system 


for i in listdir('.'):
    if i=='build/' or i == 'model.c':
        system('rm -rf %s'%(i))
    else:
        continue;
