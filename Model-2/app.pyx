 
from string import ascii_letters,digits,whitespace,punctuation
from cpython cimport array
from libc.math cimport log
from random import choice


cdef list X = (list(ascii_letters+digits+punctuation+whitespace))

cpdef matrix_determinant_3(array.array v):
    return abs(
            +v[0]*(v[4]*v[8]-v[5]*v[7])
            -v[1]*(v[3]*v[8]-v[5]*v[6])            
            +v[2]*(v[3]*v[7]-v[4]*v[6])
            );

cpdef matrix_determinant_2(array.array v):
    return abs((v[0]*v[3])-(v[1]*v[2]))



cdef gen_random_char(short int i,short int v=0):
    cdef short int x;
    if v:
        return ''.join([choice(punctuation+ascii_letters) for i in range(i)]);
    else:
        return ''.join([choice(X) for x in range(i)])


cpdef password_str_to_int(str password):
    cdef short int x;
    cdef array.array password_int = array.array('i',[]);
    for x in range(len(password)):
        password_int.append(X.index(password[x]))
    return password_int 

cpdef check_the_multi(str password):
    cdef short int length = len(password);
    cdef short int i,k,J,K 
    cdef array.array Det = array.array('i',[])
    cdef str Key;

    for i in range(1,50):
        if((length+i) % 9==0):
            k = length+i 
            break 
        else:
            pass

    Key = gen_random_char(k-length,1);
    
    J = len(Key)//choice([3,2,4]);
    K = len(password)//choice([2,3,4])
    
    Key = Key[:J]+Key[J:];
    password = password[:K]+password[K:];

    password = '%s%s'%(password,Key[::-1]);
    
    print(password)
    Det =( password_str_to_int(password));


    print(Det)
        




