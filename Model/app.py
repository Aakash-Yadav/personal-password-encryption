
from string import ascii_letters,digits,whitespace,punctuation
from math import log 
from random import choice 
from array import array
from prime import PRIME_NUMBER

class BIN_LOG_MODEL(object):
    def __init__(self):
        self.__array_of_char = list(ascii_letters+digits+whitespace+punctuation);

    def __convert_password_str_int_array(self,password):
        self.int_array_of_password = [str(self.__array_of_char.index(x))
                                     for x in password];
        self.compress = int(''.join(self.int_array_of_password));

        return self.int_array_of_password,self.compress;
    

    def return_log_value_of_int_pasword(self,password):
        _,self.password1 = self.__convert_password_str_int_array(password);
        self.result = 0
        self.log_of_password = log(self.password1);
        if self.log_of_password > 50:
            return self.log_of_password - choice(PRIME_NUMBER);
        return self.log_of_password + choice(PRIME_NUMBER);


if __name__ == '__main__':
    app = BIN_LOG_MODEL()
    print(app.return_log_value_of_int_pasword('akash-Yadav'))




